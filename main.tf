variable "TenantId" {
    type = string
}

variable "SubscriptionId" {
    type = string
}

variable "EnvironmentLocation" {
    type = string
}

provider "azurerm" {
    tenant_id = var.TenantId
    subscription_id = var.SubscriptionId

    features {}
}

resource "random_string" "storage-account-suffix" {
    length = 10
    lower = true
    upper = false
    special = false
}

resource "random_string" "storage-account-v2-suffix" {
    length = 10
    lower = true
    upper = false
    special = false
}

# Create Resource Group
resource "azurerm_resource_group" "rg" {
    name = "research-capture-storage"
    location = var.EnvironmentLocation
}

# Capture v1 Storage Account
resource "azurerm_storage_account" "storage-account" {
    name = format("%s%s", "bpcaptureea", random_string.storage-account-suffix.result)
    location = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    account_tier = "Standard"
    account_kind = "StorageV2"
    account_replication_type = "ZRS"

    enable_https_traffic_only = true

    depends_on = [ azurerm_resource_group.rg ]
}

resource "azurerm_storage_container" "logs" {
    name = "logs"
    storage_account_name = azurerm_storage_account.storage-account.name

    depends_on = [ azurerm_storage_account.storage-account ]
}

resource "azurerm_storage_container" "patches" {
    name = "patches"
    storage_account_name = azurerm_storage_account.storage-account.name

    depends_on = [ azurerm_storage_account.storage-account ]
}

resource "azurerm_storage_container" "eaarchive" {
    name = "eaarchive"
    storage_account_name = azurerm_storage_account.storage-account.name

    depends_on = [ azurerm_storage_account.storage-account ]
}


# Capture v1 Storage Account
resource "azurerm_storage_account" "storage-account-v2" {
    name = format("%s%s", "bpcapturev2", random_string.storage-account-suffix.result)
    location = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    account_tier = "Standard"
    account_kind = "StorageV2"
    account_replication_type = "ZRS"

    enable_https_traffic_only = true

    depends_on = [ azurerm_resource_group.rg ]
}

resource "azurerm_storage_container" "logs-v2" {
    name = "logs"
    storage_account_name = azurerm_storage_account.storage-account-v2.name

    depends_on = [ azurerm_storage_account.storage-account-v2 ]
}

resource "azurerm_storage_container" "patches-v2" {
    name = "patches"
    storage_account_name = azurerm_storage_account.storage-account-v2.name

    depends_on = [ azurerm_storage_account.storage-account-v2 ]
}

resource "azurerm_storage_container" "eaarchive-v2" {
    name = "eaarchive"
    storage_account_name = azurerm_storage_account.storage-account-v2.name

    depends_on = [ azurerm_storage_account.storage-account-v2 ]
}