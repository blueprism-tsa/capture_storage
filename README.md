# Blue Prism Labs Capture Storage

This repository contains Terraform code to create a storage account that collects log information from the Labs Capture software.

The code creates an Azure Storage Account and two Blob Containers - one for log information and one for distribution of patches. Access to these containers will be controlled by Secure Access Signatures (SAS) which will be created by hand.

To run the code clone the repository, check that variables contained in the `capture.tfvars` file are correct and appropriate then run the following commands:

Initialise Terraform: `terraform init`

Check execution plan: `terraform plan --var-file="capture.tfvars"`

Execute code: `terraform apply --var-file="capture.tfvars"`
